package cc.invictusgames.hub;

import cc.invictusgames.hub.manager.ManagerHandler;
import cc.invictusgames.hub.util.CC;
import cc.invictusgames.hub.util.License;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Hub extends JavaPlugin {

    private static Hub instance;
    private ManagerHandler managerHandler;

    @Override
    public void onEnable() {
        instance = this;
        managerHandler = new ManagerHandler(this);
        //license();
    }

    private void license() {
        License.ValidationType validationType = new License(
                managerHandler.getFileManager().getHubFile().getConfig().getString("LICENSE_KEY"),
                "https://license.tradez.pw/verify.php",
                this
        ).isValid();

        if(validationType != License.ValidationType.VALID) {
            sendConsoleMessage("&7------------------------------");
            sendConsoleMessage("&6iHub &7(Version: &e0.2&7)");
            sendConsoleMessage("&cLicense key not found, plugin disabling");
            sendConsoleMessage("&cBought this plugin but don't have a license key?");
            sendConsoleMessage("&cOpen up https://tradez.pw in your browser.");
            sendConsoleMessage("&7&m------------------------------");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        sendConsoleMessage("&7------------------------------");
        sendConsoleMessage("&6iHub &7(Version: &e0.2&7)");
        sendConsoleMessage("&aLicense key authenticated");
        sendConsoleMessage("&eThank your for purchasing &6iHub&e!");
        sendConsoleMessage("&eAny issues? Open up &6https://tradez.pw &ein your browser.");
        sendConsoleMessage("&7------------------------------");
    }

    public static Hub getInstance() {
        return instance;
    }

    public ManagerHandler getManagerHandler() {
        return managerHandler;
    }

    private void sendConsoleMessage(String string) {
        Bukkit.getConsoleSender().sendMessage(CC.translate(string));
    }

}
