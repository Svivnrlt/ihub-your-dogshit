package cc.invictusgames.hub.files;

import cc.invictusgames.hub.Hub;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ScoreboardFile {

    private Hub plugin;
    private File file;
    private FileConfiguration config;

    public ScoreboardFile(Hub plugin) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), "scoreboard.yml");
        this.config = YamlConfiguration.loadConfiguration(file);
    }

    public void setDefaults() {
        config.options().copyDefaults(true);

        config.addDefault("TITLE", "&6&liHub");
        config.addDefault("SPACER", "&7&m--------------------");
        config.addDefault("BOARD", Arrays.asList(
                "&eOnline Players&7:",
                "$online / 1,000",
                "",
                "&eRank&7:",
                "$rank"
        ));

        save();
    }

    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getConfig() {
        return config;
    }

}