package cc.invictusgames.hub.files;

import cc.invictusgames.hub.Hub;
import cc.invictusgames.hub.util.CC;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class HubFile {

    private Hub plugin;
    private File file;
    private FileConfiguration config;

    public HubFile(Hub plugin) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), "ihub.yml");
        this.config = YamlConfiguration.loadConfiguration(file);
    }

    public void setDefaults() {
        config.options().copyDefaults(true);

        config.addDefault("LICENSE_KEY", "1234-ABCD-5678-EFGH");
        config.addDefault("PERMISSION_PLUGIN", "Vault/FootballCore/None");

        save();
    }

    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public String getTranslatedString(String path) {
        return CC.translate(config.getString(path));
    }

    public List<String> getTranslatedStringList(String path) {
        return CC.translate(config.getStringList(path));
    }

}