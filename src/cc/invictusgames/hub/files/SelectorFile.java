package cc.invictusgames.hub.files;

import cc.invictusgames.hub.Hub;
import cc.invictusgames.hub.util.CC;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SelectorFile {

    private Hub plugin;
    private File file;
    private FileConfiguration config;

    public SelectorFile(Hub plugin) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), "selector.yml");
        this.config = YamlConfiguration.loadConfiguration(file);
    }

    public void setDefaults() {
        config.options().copyDefaults(true);

        config.addDefault("GUI.TITLE", "&eChoose a server");

        config.addDefault("SELECTOR.NAME", "&6&lServer Selector");
        config.addDefault("SELECTOR.LORE", Arrays.asList(
                "&7&m--------------------",
                "&eChoose one of our brilliant servers!",
                "&7&m--------------------"
        ));

        config.addDefault("EXAMPLE_ITEM.MATERIAL", "DIAMOND");
        config.addDefault("EXAMPLE_ITEM.NAME", "&eCentralis Example Item");
        config.addDefault("EXAMPLE_ITEM.LORE", Arrays.asList(
                "Cool!",
                "Multiple Lines!",
                "Great plugin!"
        ));
        config.addDefault("EXAMPLE_ITEM.MESSAGE", Arrays.asList(
                "Cool variables",
                "Multiple lines!"
        ));
        config.addDefault("EXAMPLE_ITEM.COMMAND", "testcommand");
        config.addDefault("EXAMPLE_ITEM.BUNGEE_SERVER", "server1");

        config.addDefault("SELECTOR.MATERIAL", "COMPASS");
        config.addDefault("SELECTOR.SLOT", 5);

        config.addDefault("ITEMS.SLOT.1.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.1.NAME", "");
        config.addDefault("ITEMS.SLOT.1.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.2.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.2.NAME", "");
        config.addDefault("ITEMS.SLOT.2.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.3.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.3.NAME", "");
        config.addDefault("ITEMS.SLOT.3.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.4.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.4.NAME", "");
        config.addDefault("ITEMS.SLOT.4.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.5.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.5.NAME", "");
        config.addDefault("ITEMS.SLOT.5.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.6.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.6.NAME", "");
        config.addDefault("ITEMS.SLOT.6.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.7.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.7.NAME", "");
        config.addDefault("ITEMS.SLOT.7.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.8.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.8.NAME", "");
        config.addDefault("ITEMS.SLOT.8.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.9.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.9.NAME", "");
        config.addDefault("ITEMS.SLOT.9.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.10.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.10.NAME", "");
        config.addDefault("ITEMS.SLOT.10.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.11.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.11.NAME", "");
        config.addDefault("ITEMS.SLOT.11.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.12.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.12.NAME", "");
        config.addDefault("ITEMS.SLOT.12.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.13.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.13.NAME", "");
        config.addDefault("ITEMS.SLOT.13.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.14.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.14.NAME", "");
        config.addDefault("ITEMS.SLOT.14.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.15.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.15.NAME", "");
        config.addDefault("ITEMS.SLOT.15.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.16.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.16.NAME", "");
        config.addDefault("ITEMS.SLOT.16.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.17.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.17.NAME", "");
        config.addDefault("ITEMS.SLOT.17.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.18.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.18.NAME", "");
        config.addDefault("ITEMS.SLOT.18.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.19.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.19.NAME", "");
        config.addDefault("ITEMS.SLOT.19.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.20.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.20.NAME", "");
        config.addDefault("ITEMS.SLOT.20.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.21.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.21.NAME", "");
        config.addDefault("ITEMS.SLOT.21.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.22.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.22.NAME", "");
        config.addDefault("ITEMS.SLOT.22.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.23.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.23.NAME", "");
        config.addDefault("ITEMS.SLOT.23.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.24.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.24.NAME", "");
        config.addDefault("ITEMS.SLOT.24.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.25.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.25.NAME", "");
        config.addDefault("ITEMS.SLOT.25.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.26.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.26.NAME", "");
        config.addDefault("ITEMS.SLOT.26.LORE", Arrays.asList("", ""));

        config.addDefault("ITEMS.SLOT.27.MATERIAL", "NOTHING");
        config.addDefault("ITEMS.SLOT.27.NAME", "");
        config.addDefault("ITEMS.SLOT.27.LORE", Arrays.asList("", ""));

        save();
    }

    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTranslatedString(String path) {
        return CC.translate(config.getString(path));
    }

    public List<String> getTranslatedStringList(String path) {
        return CC.translate(config.getStringList(path));
    }

    public FileConfiguration getConfig() {
        return config;
    }

}