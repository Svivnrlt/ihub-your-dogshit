package cc.invictusgames.hub.files;

import cc.invictusgames.hub.Hub;
import cc.invictusgames.hub.util.CC;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class MessagesFile {

    private Hub plugin;
    private File file;
    private FileConfiguration config;

    public MessagesFile(Hub plugin) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), "messages.yml");
        this.config = YamlConfiguration.loadConfiguration(file);
    }

    public void setDefaults() {
        config.options().copyDefaults(true);

        config.addDefault("WELCOME_MESSAGE", Arrays.asList(
                "&eWelcome, &6$player&e!",
                "&eYou can customize this message in the messages.yml."
        ));

        config.addDefault("SPAWN.UPDATED", "&eThe &6spawn point &ehas been updated.");
        config.addDefault("SPAWN.TELEPORTING", "&eTeleporting &6$player &eto spawn.");

        config.addDefault("BUILD_MODE.ENABLED", "&eYou are &anow &ein build mode.");
        config.addDefault("BUILD_MODE.DISABLED", "&eYou are &cno longer &ein build mode.");

        config.addDefault("ERRORS.NO_PERMISSION", "&cNo permission.");
        config.addDefault("ERRORS.PLAYER_ONLY", "&cYou can only perform this command in-game.");

        save();
    }

    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTranslatedString(String path) {
        return CC.translate(config.getString(path));
    }

    public FileConfiguration getConfig() {
        return config;
    }

}