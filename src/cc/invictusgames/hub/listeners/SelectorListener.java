package cc.invictusgames.hub.listeners;

import cc.invictusgames.hub.Hub;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class SelectorListener implements Listener {

    private Hub plugin;

    public SelectorListener(Hub plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if(event.getInventory().getName().equals(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("GUI.TITLE"))) {
            if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()) {
                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.1.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.1.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.1.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.1.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.1.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.1.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.1.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.2.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.2.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.2.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.2.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.2.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.2.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.2.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.3.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.3.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.3.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.3.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.3.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.3.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.3.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.4.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.4.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.4.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.4.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.4.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.4.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.4.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.5.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.5.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.5.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.5.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.5.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.5.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.5.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.6.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.6.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.6.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.6.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.6.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.6.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.6.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.7.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.7.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.7.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.7.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.7.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.7.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.7.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.8.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.8.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.8.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.8.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.8.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.8.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.8.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.9.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.9.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.9.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.9.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.9.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.9.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.9.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.10.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.10.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.10.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.10.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.10.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.10.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.10.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.11.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.11.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.11.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.11.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.11.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.11.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.11.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.12.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.12.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.12.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.12.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.12.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.12.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.12.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.13.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.13.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.13.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.13.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.13.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.13.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.13.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.14.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.14.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.14.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.14.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.14.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.14.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.14.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.15.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.15.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.15.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.15.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.15.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.15.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.15.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.16.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.16.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.16.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.16.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.16.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.16.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.16.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.17.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.17.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.17.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.17.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.17.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.17.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.17.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.18.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.18.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.18.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.18.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.18.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.18.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.18.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.19.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.19.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.19.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.19.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.19.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.19.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.19.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.20.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.20.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.20.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.20.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.20.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.20.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.20.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.21.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.21.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.21.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.21.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.21.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.21.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.21.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.22.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.22.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.22.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.22.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.22.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.22.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.22.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.23.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.23.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.23.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.23.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.23.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.23.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.23.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.24.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.24.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.24.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.24.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.24.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.24.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.24.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.25.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.25.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.25.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.25.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.25.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.25.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.25.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.26.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.26.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.26.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.26.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.26.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.26.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.26.BUNGEE_SERVER"),
                                player
                        );
                    }
                }

                if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.27.NAME"))) {
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.27.MESSAGE")) {
                        for(String message : plugin.getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.27.MESSAGE")) {
                            player.sendMessage(message);
                        }
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.27.COMMAND")) {
                        player.performCommand(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.27.COMMAND"));
                    }
                    if(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().isSet("ITEMS.SLOT.27.BUNGEE_SERVER")) {
                        plugin.getManagerHandler().getPluginMessagingManager().sendToServer(
                                plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.27.BUNGEE_SERVER"),
                                player
                        );
                    }
                }
            }
        }
    }

}
