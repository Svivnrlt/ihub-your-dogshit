package cc.invictusgames.hub.listeners;

import cc.invictusgames.hub.Hub;
import cc.invictusgames.hub.util.CC;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;

public class PlayerListener implements Listener {

    private Hub plugin;

    public PlayerListener(Hub plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        player.setHealth(20);
        player.setFoodLevel(20);
        player.setFireTicks(0);
        plugin.getManagerHandler().getItemManager().giveItems(player);
        plugin.getManagerHandler().getScoreboardHandler().setupBoard(player);

        for (String joinMessage : plugin.getManagerHandler().getFileManager().getMessagesFile().getConfig().getStringList("WELCOME_MESSAGE")) {
            player.sendMessage(CC.translate(joinMessage.replace("$player", player.getName())));
        }
        if (plugin.getManagerHandler().getLocationManager().getLocationsMap().containsKey("spawn")) {
            player.teleport(plugin.getManagerHandler().getLocationManager().getLocationsMap().get("spawn"));
        }
        event.setJoinMessage(null);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.getManagerHandler().getScoreboardHandler().getHelperMap().remove(player.getUniqueId());
        event.setQuitMessage(null);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (event.getMaterial() == Material.valueOf(plugin.getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("SELECTOR.MATERIAL"))) {
                player.openInventory(plugin.getManagerHandler().getSelectorManager().getSelectorInventory());
            }
        }
    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (!plugin.getManagerHandler().getListenerManager().getBuildModeMap().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (!plugin.getManagerHandler().getListenerManager().getBuildModeMap().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        event.setCancelled(true);
        if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
            if (event.getEntity() instanceof Player) {
                if (plugin.getManagerHandler().getLocationManager().getLocationsMap().containsKey("spawn")) {
                    event.getEntity().teleport(plugin.getManagerHandler().getLocationManager().getLocationsMap().get("spawn"));
                }
            }
        }
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

}
