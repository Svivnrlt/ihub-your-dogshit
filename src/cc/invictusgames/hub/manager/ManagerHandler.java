package cc.invictusgames.hub.manager;

import cc.invictusgames.hub.manager.impl.*;
import cc.invictusgames.hub.scoreboard.ScoreboardHandler;
import cc.invictusgames.hub.Hub;

public class ManagerHandler {

    private Hub plugin;
    private FileManager fileManager;
    private PluginMessagingManager pluginMessagingManager;
    private RankManager rankManager;
    private ScoreboardHandler scoreboardHandler;
    private ItemManager itemManager;
    private LocationManager locationManager;
    private ListenerManager listenerManager;

    private CommandManager commandManager;
    private SelectorManager selectorManager;

    public ManagerHandler(Hub plugin) {
        this.plugin = plugin;
        load();
    }

    private void load() {
        fileManager = new FileManager(this);
        pluginMessagingManager = new PluginMessagingManager(this);
        rankManager = new RankManager(this);
        scoreboardHandler = new ScoreboardHandler(this);
        itemManager = new ItemManager(this);
        locationManager = new LocationManager(this);
        listenerManager = new ListenerManager(this);
        commandManager = new CommandManager(this);
        selectorManager = new SelectorManager(this);
    }

    public Hub getPlugin() {
        return plugin;
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public ScoreboardHandler getScoreboardHandler() {
        return scoreboardHandler;
    }

    public ItemManager getItemManager() {
        return itemManager;
    }

    public ListenerManager getListenerManager() {
        return listenerManager;
    }

    public RankManager getRankManager() {
        return rankManager;
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public SelectorManager getSelectorManager() {
        return selectorManager;
    }

    public PluginMessagingManager getPluginMessagingManager() {
        return pluginMessagingManager;
    }
}