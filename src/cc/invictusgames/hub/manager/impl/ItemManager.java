package cc.invictusgames.hub.manager.impl;

import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import cc.invictusgames.hub.util.Items;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ItemManager extends Manager {

    private Map<String, Integer> slotsMap;

    public ItemManager(ManagerHandler managerHandler) {
        super(managerHandler);
        slotsMap = new HashMap<>();
        load();
    }

    private void load() {
        slotsMap.put("SELECTOR", managerHandler.getFileManager().getSelectorFile().getConfig().getInt("SELECTOR.SLOT") -1);
    }

    public void giveItems(Player player) {
        player.getInventory().setItem(slotsMap.get("SELECTOR"), Items.SERVER_SELECTOR.getItemStack());
    }

    public Map<String, Integer> getSlotsMap() {
        return slotsMap;
    }
}
