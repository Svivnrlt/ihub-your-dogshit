package cc.invictusgames.hub.manager.impl;

import cc.invictusgames.hub.files.SelectorFile;
import cc.invictusgames.hub.files.HubFile;
import cc.invictusgames.hub.files.MessagesFile;
import cc.invictusgames.hub.files.ScoreboardFile;
import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;

public class FileManager extends Manager {

    private HubFile hubFile;
    private MessagesFile messagesFile;
    private ScoreboardFile scoreboardFile;
    private SelectorFile selectorFile;

    public FileManager(ManagerHandler managerHandler) {
        super(managerHandler);
        load();
    }

    private void load() {
        hubFile = new HubFile(managerHandler.getPlugin());
        hubFile.setDefaults();
        messagesFile = new MessagesFile(managerHandler.getPlugin());
        messagesFile.setDefaults();
        scoreboardFile = new ScoreboardFile(managerHandler.getPlugin());
        scoreboardFile.setDefaults();
        selectorFile = new SelectorFile(managerHandler.getPlugin());
        selectorFile.setDefaults();
    }

    public HubFile getHubFile() {
        return hubFile;
    }

    public MessagesFile getMessagesFile() {
        return messagesFile;
    }

    public ScoreboardFile getScoreboardFile() {
        return scoreboardFile;
    }

    public SelectorFile getSelectorFile() {
        return selectorFile;
    }
}
