package cc.invictusgames.hub.manager.impl;

import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;

public class LocationManager extends Manager {

    private Map<String, Location> locationsMap;

    public LocationManager(ManagerHandler managerHandler) {
        super(managerHandler);
        locationsMap = new HashMap<>();
        load();
    }

    private void load() {
        try {
            String worldName = managerHandler.getFileManager().getHubFile().getConfig().getString("SPAWN.WORLD_NAME");
            double x = managerHandler.getFileManager().getHubFile().getConfig().getDouble("SPAWN.X");
            double y = managerHandler.getFileManager().getHubFile().getConfig().getDouble("SPAWN.Y");
            double z = managerHandler.getFileManager().getHubFile().getConfig().getDouble("SPAWN.Z");
            float yaw = managerHandler.getFileManager().getHubFile().getConfig().getFloat("SPAWN.YAW");
            float pitch = managerHandler.getFileManager().getHubFile().getConfig().getFloat("SPAWN.PITCH");
            Location spawn = new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
            locationsMap.put("spawn", spawn);
        } catch (Exception e) {
            System.out.println("[iHub] Could not load spawn location.");
        }
    }

    public Map<String, Location> getLocationsMap() {
        return locationsMap;
    }

}
