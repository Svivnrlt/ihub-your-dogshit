package cc.invictusgames.hub.manager.impl;

import cc.invictusgames.hub.listeners.PlayerListener;
import cc.invictusgames.hub.listeners.SelectorListener;
import cc.invictusgames.hub.listeners.WorldListener;
import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ListenerManager extends Manager {

    private List<UUID> buildModeMap;

    public ListenerManager(ManagerHandler managerHandler) {
        super(managerHandler);
        buildModeMap = new ArrayList<>();
        manager = Bukkit.getPluginManager();
        load();
    }

    private PluginManager manager;

    private void load() {
        manager.registerEvents(new PlayerListener(managerHandler.getPlugin()), managerHandler.getPlugin());
        manager.registerEvents(new WorldListener(managerHandler.getPlugin()), managerHandler.getPlugin());
        manager.registerEvents(new SelectorListener(managerHandler.getPlugin()), managerHandler.getPlugin());
    }

    public List<UUID> getBuildModeMap() {
        return buildModeMap;
    }

}
