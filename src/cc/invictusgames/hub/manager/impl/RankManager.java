package cc.invictusgames.hub.manager.impl;

import com.verispvp.core.api.CoreAPI;
import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class RankManager extends Manager {

    private String rankPlugin;
    private Permission permission;

    public RankManager(ManagerHandler managerHandler) {
        super(managerHandler);
        this.rankPlugin = managerHandler.getFileManager().getHubFile().getConfig().getString("PERMISSION_PLUGIN").toUpperCase();
        if(rankPlugin.equals("VAULT")) {
            setupPermissions();
        }
    }

    public String getRank(Player player) {
        switch(rankPlugin) {
            case "VAULT":
                return permission.getPrimaryGroup(player);
            case "FOOTBALLCORE":
                return CoreAPI.INSTANCE.getRank(player).getColor() + CoreAPI.INSTANCE.getRank(player).getName();
            case "NONE":
                return null;
            default:
                System.out.println("[Hub] Failed to regonize a valid rank plugin, please set this up in your centralis.yml");
                return "Set this up in centralis.yml";
        }
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager()
                .getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

}
