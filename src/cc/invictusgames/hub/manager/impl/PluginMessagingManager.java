package cc.invictusgames.hub.manager.impl;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class PluginMessagingManager extends Manager implements PluginMessageListener {

    public int all = 0;

    public PluginMessagingManager(ManagerHandler managerHandler) {
        super(managerHandler);
        load();
    }

    private void load() {
        Bukkit.getMessenger().registerOutgoingPluginChannel(managerHandler.getPlugin(), "BungeeCord");
        Bukkit.getMessenger().registerIncomingPluginChannel(managerHandler.getPlugin(), "BungeeCord", this);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(managerHandler.getPlugin(), () -> {
            for (Player all : Bukkit.getServer().getOnlinePlayers()) {
                checkALLCount(all);
            }
        }, 50L, 50L);
    }

    public void sendToServer(String server, Player player) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        player.sendPluginMessage(managerHandler.getPlugin(), "BungeeCord", out.toByteArray());
    }

    private void checkALLCount(Player player) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PlayerCount");
        out.writeUTF("ALL");
        player.sendPluginMessage(managerHandler.getPlugin(), "BungeeCord", out.toByteArray());
    }


    public void onPluginMessageReceived(String s, Player player, byte[] bytes) {
        if (s.equals("BungeeCord")) {
            ByteArrayDataInput byteArrayDataInput = ByteStreams.newDataInput(bytes);
            String subchannel = byteArrayDataInput.readUTF();
            if (subchannel.equals("PlayerCount")) {
                String server = byteArrayDataInput.readUTF();
                try {
                    if (server.equalsIgnoreCase("ALL")) {
                        all = byteArrayDataInput.readInt();
                    }
                } catch (Exception ex) {
                    all = -1;
                }
            }
        }
    }

}
