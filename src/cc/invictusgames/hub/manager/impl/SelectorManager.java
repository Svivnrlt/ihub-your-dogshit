package cc.invictusgames.hub.manager.impl;

import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import cc.invictusgames.hub.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public class SelectorManager extends Manager {

    private Inventory serverSelector;

    public SelectorManager(ManagerHandler managerHandler) {
        super(managerHandler);
        load();
    }

    private void load() {
        serverSelector = Bukkit.createInventory(null, 27, managerHandler.getFileManager().getSelectorFile().getTranslatedString("GUI.TITLE"));

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.1.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(0, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.1.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.1.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.1.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.2.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(1, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.2.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.2.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.2.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.3.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(2, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.3.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.3.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.3.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.4.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(3, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.4.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.4.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.4.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.5.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(4, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.5.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.5.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.5.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.6.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(5, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.6.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.6.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.6.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.7.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(6, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.7.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.7.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.7.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.8.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(7, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.8.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.8.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.8.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.9.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(8, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.9.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.9.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.9.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.10.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(9, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.10.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.10.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.10.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.11.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(10, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.11.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.11.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.11.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.12.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(11, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.12.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.12.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.12.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.13.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(12, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.13.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.13.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.13.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.14.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(13, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.14.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.14.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.14.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.15.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(14, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.15.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.15.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.15.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.16.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(15, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.16.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.16.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.16.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.17.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(16, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.17.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.17.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.17.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.18.MATERIAL").equalsIgnoreCase("NOTHING")) {
            serverSelector.setItem(17, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.18.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.18.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.18.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.19.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(18, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.19.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.19.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.19.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.20.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(19, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.20.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.20.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.20.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.21.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(20, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.21.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.21.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.21.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.22.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(21, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.22.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.22.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.22.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.23.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(22, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.23.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.23.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.23.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.24.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(23, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.24.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.24.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.24.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.25.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(24, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.25.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.25.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.25.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.26.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(25, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.26.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.26.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.26.LORE"))
                    .toItemStack());
        }

        if(!managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.27.MATERIAL").equalsIgnoreCase( "NOTHING")) {
            serverSelector.setItem(26, new ItemBuilder(
                    Material.valueOf(managerHandler.getFileManager().getSelectorFile().getConfig().getString("ITEMS.SLOT.27.MATERIAL")))
                    .setName(managerHandler.getFileManager().getSelectorFile().getTranslatedString("ITEMS.SLOT.27.NAME"))
                    .setLore(managerHandler.getFileManager().getSelectorFile().getTranslatedStringList("ITEMS.SLOT.27.LORE"))
                    .toItemStack());
        }
        
    }

    public Inventory getSelectorInventory() {
        return serverSelector;
    }

}
