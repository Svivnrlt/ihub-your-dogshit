package cc.invictusgames.hub.manager.impl;

import cc.invictusgames.hub.commands.BuildModeCommand;
import cc.invictusgames.hub.commands.SetSpawnCommand;
import cc.invictusgames.hub.commands.SpawnCommand;
import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;

public class CommandManager extends Manager {

    public CommandManager(ManagerHandler managerHandler) {
        super(managerHandler);
        load();
    }

    private void load() {
        managerHandler.getPlugin().getCommand("setspawn").setExecutor(new SetSpawnCommand(managerHandler.getPlugin()));
        managerHandler.getPlugin().getCommand("spawn").setExecutor(new SpawnCommand(managerHandler.getPlugin()));
        managerHandler.getPlugin().getCommand("buildmode").setExecutor(new BuildModeCommand(managerHandler.getPlugin()));
    }

}
