package cc.invictusgames.hub.scoreboard;

import cc.invictusgames.hub.manager.Manager;
import cc.invictusgames.hub.manager.ManagerHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ScoreboardHandler extends Manager {

    private HashMap<UUID, ScoreboardHelper> helperMap;
    private String title;

    public ScoreboardHandler(ManagerHandler managerHandler) {
        super(managerHandler);
        helperMap = new HashMap<>();
        title = managerHandler.getFileManager().getScoreboardFile().getConfig().getString("TITLE");
        register();
    }

    public void register() {
        new BukkitRunnable() {
            public void run() {
                for (Map.Entry<UUID, ScoreboardHelper> entry : ScoreboardHandler.this.helperMap.entrySet()) {
                    Player player = Bukkit.getPlayer(entry.getKey());
                    if (player != null) {
                        ScoreboardHelper entries = entry.getValue();
                        entries.clear();

                        entries.add(managerHandler.getFileManager().getScoreboardFile().getConfig().getString("SPACER"));
                        for(String boardEntries : managerHandler.getFileManager().getScoreboardFile().getConfig().getStringList("BOARD")) {
                            entries.add(boardEntries
                                    .replace("$online", String.valueOf(managerHandler.getPluginMessagingManager().all))
                                    .replace("$rank", managerHandler.getRankManager().getRank(player))
                            );
                        }
                        entries.add(managerHandler.getFileManager().getScoreboardFile().getConfig().getString("SPACER"));

                        entries.update(player);
                    }
                }
            }
        }.runTaskTimerAsynchronously(managerHandler.getPlugin(), 50L, 50L);
    }

    public void setupBoard(Player player) {
        if (player != null) {
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            ScoreboardHelper scoreboardHelper = new ScoreboardHelper(scoreboard, title);
            this.helperMap.put(player.getUniqueId(), scoreboardHelper);
        }
    }

    public Map<UUID, ScoreboardHelper> getHelperMap() {
        return helperMap;
    }
}
