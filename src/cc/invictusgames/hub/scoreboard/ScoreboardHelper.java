package cc.invictusgames.hub.scoreboard;

import cc.invictusgames.hub.util.CC;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

public class ScoreboardHelper {

    private List<Entry> list;
    private Scoreboard board;
    private Objective o;
    private String tag;
    private int lastSentCount;

    public ScoreboardHelper(Scoreboard board,  String title) {
        this.list = new ArrayList<>();
        this.lastSentCount = -1;
        this.tag = CC.translate(title);
        this.board = board;
        (this.o = this.getOrCreateObjective(this.tag)).setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public List<Entry> getEntries() {
        return this.list;
    }

    public void add(String string) {
        string = CC.translate(string);
        Entry text;
        if (string.length() <= 15) {
            text = new Entry(string, "");
        } else {
            String first = string.substring(0, 16);
            String second = string.substring(16, string.length());
            if (first.endsWith(String.valueOf('§'))) {
                first = first.substring(0, first.length() - 1);
                second = '§' + second;
            }
            String lastColors = ChatColor.getLastColors(first);
            second = lastColors + second;
            text = new Entry(first, StringUtils.left(second, 16));
        }
        this.list.add(text);
    }

    public void clear() {
        this.list.clear();
    }

    public void remove(int i) {
        String name = this.getNameForIndex(i);
        this.board.resetScores(name);
        Team t = this.getOrCreateTeam(String.valueOf(ChatColor.stripColor(StringUtils.left(this.tag, 14))) + i,
                i);
        t.unregister();
    }

    public void update(Player player) {
        player.setScoreboard(this.board);
        for (int sentCount = 0; sentCount < this.list.size(); ++sentCount) {
            Team i = this.getOrCreateTeam(
                    String.valueOf(ChatColor.stripColor(StringUtils.left(this.tag, 14))) + sentCount, sentCount);
            Entry str = this.list.get(this.list.size() - sentCount - 1);
            i.setPrefix(str.getLeft());
            i.setSuffix(str.getRight());
            this.o.getScore(this.getNameForIndex(sentCount)).setScore(sentCount + 1);
        }
        if (this.lastSentCount != -1) {
            for (int sentCount = this.list.size(), j = 0; j < this.lastSentCount - sentCount; ++j) {
                this.remove(sentCount + j);
            }
        }
        this.lastSentCount = this.list.size();
    }

    public Team getOrCreateTeam(String team, int i) {
        Team boardTeam = this.board.getTeam(team);
        if (boardTeam == null) {
            boardTeam = this.board.registerNewTeam(team);
            boardTeam.addEntry(this.getNameForIndex(i));
        }
        return boardTeam;
    }

    public Objective getOrCreateObjective(String boardObjective) {
        Objective value = this.board.getObjective("dummy");
        if (value == null) {
            value = this.board.registerNewObjective("dummyboard", "tradez");
        }
        value.setDisplayName(boardObjective);
        return value;
    }

    public String getNameForIndex(int i) {
        return ChatColor.values()[i].toString() + CC.RESET;
    }

    private class Entry {
        private String left;
        private String right;

        public Entry( String left,  String right) {
            this.left = left;
            this.right = right;
        }

        public String getLeft() {
            return this.left;
        }

        public String getRight() {
            return this.right;
        }
    }
}