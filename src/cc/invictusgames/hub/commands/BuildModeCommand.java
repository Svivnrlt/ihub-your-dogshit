package cc.invictusgames.hub.commands;

import cc.invictusgames.hub.Hub;
import cc.invictusgames.hub.util.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildModeCommand implements CommandExecutor {

    private Hub plugin;

    public BuildModeCommand(Hub plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage(Messages.PLAYER_ONLY.get());
            return true;
        }

        if(!sender.hasPermission("centralis.command.buildmode")) {
            sender.sendMessage(Messages.NO_PERMISSION.get());
            return true;
        }

        Player player = (Player) sender;

        if(plugin.getManagerHandler().getListenerManager().getBuildModeMap().contains(player.getUniqueId())) {
            plugin.getManagerHandler().getListenerManager().getBuildModeMap().remove(player.getUniqueId());
            sender.sendMessage(Messages.BUILD_MODE_DISABLED.get());
            return true;
        }

        plugin.getManagerHandler().getListenerManager().getBuildModeMap().add(player.getUniqueId());
        sender.sendMessage(Messages.BUILD_MODE_ENABLED.get());
        return true;
    }

}
