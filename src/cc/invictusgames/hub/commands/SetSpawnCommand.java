package cc.invictusgames.hub.commands;

import cc.invictusgames.hub.util.Messages;
import cc.invictusgames.hub.Hub;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnCommand implements CommandExecutor {

    private Hub plugin;

    public SetSpawnCommand(Hub plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage(Messages.PLAYER_ONLY.get());
            return true;
        }

        if(!sender.hasPermission("centralis.command.setspawn")) {
            sender.sendMessage(Messages.NO_PERMISSION.get());
            return true;
        }

        Player player = (Player) sender;

        plugin.getManagerHandler().getFileManager().getHubFile().getConfig().set("SPAWN.WORLD_NAME", player.getWorld().getName());
        plugin.getManagerHandler().getFileManager().getHubFile().getConfig().set("SPAWN.X", player.getLocation().getX());
        plugin.getManagerHandler().getFileManager().getHubFile().getConfig().set("SPAWN.Y", player.getLocation().getY());
        plugin.getManagerHandler().getFileManager().getHubFile().getConfig().set("SPAWN.Z", player.getLocation().getZ());
        plugin.getManagerHandler().getFileManager().getHubFile().getConfig().set("SPAWN.YAW", player.getLocation().getYaw());
        plugin.getManagerHandler().getFileManager().getHubFile().getConfig().set("SPAWN.PITCH", player.getLocation().getPitch());
        plugin.getManagerHandler().getFileManager().getHubFile().save();
        String worldName = plugin.getManagerHandler().getFileManager().getHubFile().getConfig().getString("SPAWN.WORLD_NAME");
        double x = plugin.getManagerHandler().getFileManager().getHubFile().getConfig().getDouble("SPAWN.X");
        double y = plugin.getManagerHandler().getFileManager().getHubFile().getConfig().getDouble("SPAWN.Y");
        double z = plugin.getManagerHandler().getFileManager().getHubFile().getConfig().getDouble("SPAWN.Z");
        float yaw = plugin.getManagerHandler().getFileManager().getHubFile().getConfig().getFloat("SPAWN.YAW");
        float pitch = plugin.getManagerHandler().getFileManager().getHubFile().getConfig().getFloat("SPAWN.PITCH");
        Location spawn = new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
        plugin.getManagerHandler().getLocationManager().getLocationsMap().put("spawn", spawn);
        sender.sendMessage(Messages.SPAWN_POINT_UPDATED.get());
        return true;
    }

}
