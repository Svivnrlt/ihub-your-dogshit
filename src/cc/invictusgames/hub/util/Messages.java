package cc.invictusgames.hub.util;

import cc.invictusgames.hub.Hub;

public enum Messages {

    NO_PERMISSION("ERROR.NO_PERMISSION"),
    PLAYER_ONLY("ERROR.PLAYER_ONLY"),
    BUILD_MODE_ENABLED("BUILD_MODE.ENABLED"),
    BUILD_MODE_DISABLED("BUILD_MODE.DISABLED"),
    SPAWN_POINT_UPDATED("SPAWN.UPDATED"),
    TELEPORTING_TO_SPAWN("SPAWN.TELEPORTING");

    private String path;

    Messages(String path) {
        this.path = path;
    }

    public String get() {
        return Hub.getInstance().getManagerHandler().getFileManager().getMessagesFile().getTranslatedString(path);
    }

}
