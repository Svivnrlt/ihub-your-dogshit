package cc.invictusgames.hub.util;

import cc.invictusgames.hub.Hub;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Items {

    SERVER_SELECTOR(new ItemBuilder(
            Material.valueOf(Hub.getInstance().getManagerHandler().getFileManager().getSelectorFile().getConfig().getString("SELECTOR.MATERIAL")))
            .setName(Hub.getInstance().getManagerHandler().getFileManager().getSelectorFile().getTranslatedString("SELECTOR.NAME"))
            .setLore(Hub.getInstance().getManagerHandler().getFileManager().getSelectorFile().getTranslatedStringList("SELECTOR.LORE"))
            .toItemStack());


    private ItemStack itemStack;

    private Items(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

}
